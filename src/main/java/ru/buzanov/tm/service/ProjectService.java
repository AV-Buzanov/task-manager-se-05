package ru.buzanov.tm.service;

import ru.buzanov.tm.bootstrap.Bootstrap;
import ru.buzanov.tm.entity.Project;
import ru.buzanov.tm.repository.ProjectRepository;
import ru.buzanov.tm.repository.TaskRepository;

import java.util.Collection;

public class ProjectService {

    private ProjectRepository projectRepository;

    private TaskRepository taskRepository;

    private Bootstrap bootstrap;

    public ProjectService() {
    }

    public ProjectService(Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
        this.taskRepository = bootstrap.getTaskRepository();
        this.projectRepository = bootstrap.getProjectRepository();
    }

    public Project persist() {
        return projectRepository.persist();
    }

    public Collection<Project> findAll() {
        return projectRepository.findAll();
    }

    public Project findOne(String id) throws Exception {
        if (id==null||id.isEmpty())
            return null;
        return projectRepository.findOne(id);
    }

    public void merge(String id, Project project) {
        if (project==null||id==null||id.isEmpty())
            return;
        projectRepository.merge(id, project);
    }

    public Project remove(String id) {
        if (id==null||id.isEmpty())
            return null;
        return projectRepository.remove(id);
    }

    public void removeAll() {
        projectRepository.removeAll();
    }

    public boolean isNameExist(String name) {
        return projectRepository.isNameExist(name);
    }

    public String getList() {
        return projectRepository.getList();
    }

    public String getIdByCount(int count) {
        return projectRepository.getIdByCount(count);
    }
}