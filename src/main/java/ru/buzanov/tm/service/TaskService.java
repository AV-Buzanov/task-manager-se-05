package ru.buzanov.tm.service;

import ru.buzanov.tm.bootstrap.Bootstrap;
import ru.buzanov.tm.entity.Task;
import ru.buzanov.tm.repository.ProjectRepository;
import ru.buzanov.tm.repository.TaskRepository;

import java.util.Collection;

public class TaskService {

    private Bootstrap bootstrap;
    private TaskRepository taskRepository;
    private ProjectRepository projectRepository;

    public TaskService() {
    }

    public TaskService(Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
        this.taskRepository = bootstrap.getTaskRepository();
        this.projectRepository = bootstrap.getProjectRepository();
    }

    public Task persist() {
        return taskRepository.persist();
    }

    public Collection<Task> findAll() {
        return taskRepository.findAll();
    }

    public Task findOne(String id) {
        if (id==null||id.isEmpty())
            return null;
        return taskRepository.findOne(id);
    }

    public void merge(String id, Task task) {
        if (id==null||task==null||id.isEmpty())
            return;
        taskRepository.merge(id, task);
    }

    public Task remove(String id) {
        if (id==null||id.isEmpty())
            return null;
        return taskRepository.remove(id);
    }

    public void removeAll() {
        taskRepository.removeAll();
    }

    public void removeByProjectId(String projectId) {
        if (projectId==null||projectId.isEmpty())
            return;
        taskRepository.removeByProjectId(projectId);
    }

    public boolean isNameExist(String name) {
        if (name==null||name.isEmpty())
            return false;
        return taskRepository.isNameExist(name);
    }

    public String getList() {
        return taskRepository.getList();
    }

    public String getIdByCount(int count) {
        return taskRepository.getIdByCount(count);
    }

    public Collection<Task> findByProjectId(String projectId) {
        if (projectId==null||projectId.isEmpty())
            return null;
        return taskRepository.findByProjectId(projectId);
    }
}