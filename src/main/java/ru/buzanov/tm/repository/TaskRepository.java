package ru.buzanov.tm.repository;

import ru.buzanov.tm.entity.Task;

import java.util.*;


public class TaskRepository {
    private Map<String, Task> tasks = new LinkedHashMap<>();

    public Task persist() {
        Task task = new Task();
        tasks.put(task.getId(), task);
        return task;
    }

    public Collection<Task> findAll() {
        return tasks.values();
    }

    public Task findOne(String id) {
        if (tasks.containsKey(id))
            return tasks.get(id);
        else return null;
    }

    public Collection<Task> findByProjectId(String projectId) {
        Collection<Task> list = new ArrayList<>();
        for (Task task : findAll()) {
            if (projectId.equals(task.getProjectId()))
                list.add(task);
        }
        return list;
    }

    public boolean isNameExist(String name) {
        for (Task task : findAll()) {
            if (task.getName().equals(name))
                return true;
        }
        return false;
    }

    public String getList() {
        int indexBuf = 1;
        StringBuilder s = new StringBuilder();
        for (Task task : findAll()) {
            s.append(indexBuf).append(". ").append(task.getName());
            if (findAll().size() > indexBuf)
                s.append(System.getProperty("line.separator"));
            indexBuf++;
        }
        return s.toString();
    }

    public String getIdByCount(int count) {
        int indexBuf = 1;
        for (Task task : findAll()) {
            if (indexBuf == count)
                return task.getId();
            indexBuf++;
        }
        return null;
    }

    public void merge(String id, Task task) {
        if (task.getName() != null)
            tasks.get(id).setName(task.getName());
        if (task.getStartDate() != null)
            tasks.get(id).setStartDate(task.getStartDate());
        if (task.getEndDate() != null)
            tasks.get(id).setEndDate(task.getEndDate());
        if (task.getDescription() != null)
            tasks.get(id).setDescription(task.getDescription());
        if (task.getProjectId() != null)
            tasks.get(id).setProjectId(task.getProjectId());
    }

    public Task remove(String id) {
        return tasks.remove(id);
    }

    public void removeAll() {
        tasks.clear();
    }

    public void removeByProjectId(String projectId) {
        Iterator it = tasks.values().iterator();
        while (it.hasNext()) {
            Task task = (Task) it.next();
            if (projectId.equals(task.getProjectId()))
                it.remove();
        }
    }
}
