package ru.buzanov.tm.command;

import ru.buzanov.tm.bootstrap.Bootstrap;
import ru.buzanov.tm.constant.FormatConst;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.Locale;

public abstract class AbstractCommand {
    protected Bootstrap bootstrap;
    protected BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
    protected SimpleDateFormat dateFormat = new SimpleDateFormat(FormatConst.DATE_FORMAT, Locale.ENGLISH);

    public void setBootstrap(Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    public abstract String command();

    public abstract String description();

    public abstract void execute() throws Exception;
}
