package ru.buzanov.tm.command;

import ru.buzanov.tm.constant.FormatConst;
import ru.buzanov.tm.entity.Task;

public class TaskViewCommand extends AbstractCommand {
    @Override
    public String command() {
        return "task-view";
    }

    @Override
    public String description() {
        return "View task information";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[CHOOSE TASK TO VIEW]");
        System.out.println(bootstrap.getTaskService().getList());
        String idBuf = bootstrap.getTaskService().getIdByCount(Integer.parseInt(reader.readLine()));
        Task task = bootstrap.getTaskService().findOne(idBuf);
        System.out.println("[NAME]");
        System.out.println(task.getName());
        System.out.println("[START DATE]");
        if (task.getStartDate() != null)
            System.out.println(dateFormat.format(task.getStartDate()));
        else
            System.out.println(FormatConst.EMPTY_FIELD);
        System.out.println("[END DATE]");
        if (task.getEndDate() != null)
            System.out.println(dateFormat.format(task.getEndDate()));
        else
            System.out.println(FormatConst.EMPTY_FIELD);
        System.out.println("[DESCRIPTION]");
        if (task.getDescription() != null)
            System.out.println(task.getDescription());
        else
            System.out.println(FormatConst.EMPTY_FIELD);
        System.out.println("[PROJECT]");
        if (task.getProjectId() != null)
            System.out.println(bootstrap.getProjectService().findOne(task.getProjectId()).getName());
        else
            System.out.println(FormatConst.EMPTY_FIELD);
    }
}
