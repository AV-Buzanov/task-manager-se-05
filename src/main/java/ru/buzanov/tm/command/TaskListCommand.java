package ru.buzanov.tm.command;

public class TaskListCommand extends AbstractCommand {
    @Override
    public String command() {
        return "task-list";
    }

    @Override
    public String description() {
        return "Show all tasks.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[TASK LIST]");
        System.out.println(bootstrap.getTaskService().getList());
    }
}
