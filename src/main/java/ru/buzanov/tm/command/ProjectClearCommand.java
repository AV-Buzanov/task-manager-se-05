package ru.buzanov.tm.command;

import ru.buzanov.tm.entity.Project;

public class ProjectClearCommand extends AbstractCommand {
    @Override
    public String command() {
        return "project-clear";
    }

    @Override
    public String description() {
        return "Remove all projects.";
    }

    @Override
    public void execute() throws Exception {
        for (Project project : bootstrap.getProjectService().findAll())
            bootstrap.getTaskService().removeByProjectId(project.getId());
        bootstrap.getProjectService().removeAll();
        System.out.println("[ALL PROJECTS REMOVED WITH CONNECTED TASKS]");
    }
}
