package ru.buzanov.tm.command;

import ru.buzanov.tm.constant.FormatConst;
import ru.buzanov.tm.entity.Project;

public class ProjectEditCommand extends AbstractCommand {
    @Override
    public String command() {
        return "project-edit";
    }

    @Override
    public String description() {
        return "Edit project";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[CHOOSE PROJECT TO EDIT]");
        System.out.println(bootstrap.getProjectService().getList());
        String stringBuf;
        stringBuf = bootstrap.getProjectService().getIdByCount(Integer.parseInt(reader.readLine()));
        Project project = bootstrap.getProjectService().findOne(stringBuf);
        System.out.println("[NAME]");
        System.out.println(project.getName());
        System.out.println("[ENTER NEW NAME]");
        stringBuf = reader.readLine();
        if (bootstrap.getProjectService().isNameExist(stringBuf)) {
            System.out.println("Project with this name already exist.");
            return;
        }
        if (!stringBuf.isEmpty())
            project.setName(stringBuf);

        System.out.println("[START DATE]");
        if (project.getStartDate() != null)
            System.out.println(dateFormat.format(project.getStartDate()));
        else
            System.out.println(FormatConst.EMPTY_FIELD);
        System.out.println("[ENTER NEW START DATE]");
        stringBuf = reader.readLine();
        if (!stringBuf.isEmpty())
            project.setStartDate(dateFormat.parse(stringBuf));
        System.out.println("[END DATE]");
        if (project.getEndDate() != null)
            System.out.println(dateFormat.format(project.getEndDate()));
        else
            System.out.println(FormatConst.EMPTY_FIELD);
        System.out.println("[ENTER NEW END DATE]");
        stringBuf = reader.readLine();
        if (!stringBuf.isEmpty())
            project.setEndDate(dateFormat.parse(stringBuf));
        System.out.println("[DESCRIPTION]");
        if (project.getDescription() != null)
            System.out.println(project.getDescription());
        else
            System.out.println(FormatConst.EMPTY_FIELD);
        System.out.println("[ENTER NEW DESCRIPTION]");
        stringBuf = reader.readLine();
        if (!stringBuf.isEmpty())
            project.setDescription(stringBuf);
        bootstrap.getProjectService().merge(project.getId(), project);
        System.out.println("[OK]");
    }
}
