package ru.buzanov.tm.command;

public class ProjectRemoveCommand extends AbstractCommand {
    @Override
    public String command() {
        return "project-remove";
    }

    @Override
    public String description() {
        return "Remove selected project with connected tasks.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[CHOOSE PROJECT TO REMOVE]");
        System.out.println(bootstrap.getProjectService().getList());
        String idBuf = bootstrap.getProjectService().getIdByCount(Integer.parseInt(reader.readLine()));
        bootstrap.getProjectService().remove(idBuf);
        bootstrap.getTaskService().removeByProjectId(idBuf);
        System.out.println("[OK]");
    }
}
