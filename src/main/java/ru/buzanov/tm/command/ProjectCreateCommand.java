package ru.buzanov.tm.command;

import ru.buzanov.tm.constant.FormatConst;
import ru.buzanov.tm.entity.Project;

public class ProjectCreateCommand extends AbstractCommand {
    @Override
    public String command() {
        return "project-create";
    }

    @Override
    public String description() {
        return "Create new project.";
    }

    @Override
    public void execute() throws Exception {
        Project project = new Project();
        String stringBuf;
        System.out.println("[PROJECT CREATE]");
        System.out.println("[ENTER NAME]");
        stringBuf = reader.readLine();
        if (stringBuf.isEmpty()) {
            System.out.println("Project name can't be empty");
            return;
        }
        if (bootstrap.getProjectService().isNameExist(stringBuf)) {
            System.out.println("Project with this name already exist.");
            return;
        }
        project.setName(stringBuf);
        System.out.println("[ENTER PROJECT START DATE " + FormatConst.DATE_FORMAT + " ]");
        stringBuf = reader.readLine();
        if (!stringBuf.isEmpty())
            project.setStartDate(dateFormat.parse(stringBuf));
        System.out.println("[ENTER PROJECT END DATE " + FormatConst.DATE_FORMAT + " ]");
        stringBuf = reader.readLine();
        if (!stringBuf.isEmpty())
            project.setEndDate(dateFormat.parse(stringBuf));
        System.out.println("[ENTER DESCRIPTION]");
        stringBuf = reader.readLine();
        if (!stringBuf.isEmpty())
            project.setDescription(stringBuf);
        stringBuf = bootstrap.getProjectService().persist().getId();
        bootstrap.getProjectService().merge(stringBuf, project);
        System.out.println("[OK]");
    }
}
