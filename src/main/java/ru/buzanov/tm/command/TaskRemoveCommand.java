package ru.buzanov.tm.command;

public class TaskRemoveCommand extends AbstractCommand {
    @Override
    public String command() {
        return "task-remove";
    }

    @Override
    public String description() {
        return "Remove selected task.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[CHOOSE TASK TO REMOVE]");
        System.out.println(bootstrap.getTaskService().getList());
        String idBuf = bootstrap.getTaskService().getIdByCount(Integer.parseInt(reader.readLine()));
        bootstrap.getTaskService().remove(idBuf);
        System.out.println("[OK]");
    }
}
