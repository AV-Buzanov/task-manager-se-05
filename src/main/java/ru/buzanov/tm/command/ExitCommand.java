package ru.buzanov.tm.command;

public class ExitCommand extends AbstractCommand {
    @Override
    public String command() {
        return "exit";
    }

    @Override
    public String description() {
        return "Close application.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[GOODBYE! TILL WE MEET AGAIN!]");
    }
}
