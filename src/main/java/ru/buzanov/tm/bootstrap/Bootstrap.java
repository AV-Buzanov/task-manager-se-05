package ru.buzanov.tm.bootstrap;

import ru.buzanov.tm.command.*;
import ru.buzanov.tm.repository.ProjectRepository;
import ru.buzanov.tm.repository.TaskRepository;
import ru.buzanov.tm.service.ProjectService;
import ru.buzanov.tm.service.TaskService;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

public class Bootstrap {
    private final BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
    private final ProjectRepository projectRepository = new ProjectRepository();
    private final TaskRepository taskRepository = new TaskRepository();
    private final ProjectService projectService = new ProjectService(this);
    private final TaskService taskService = new TaskService(this);
    private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();

    private void init(){
        registryCommand(new HelpCommand());
        registryCommand(new ProjectCreateCommand());
        registryCommand(new ProjectListCommand());
        registryCommand(new ProjectViewCommand());
        registryCommand(new ProjectEditCommand());
        registryCommand(new ProjectRemoveCommand());
        registryCommand(new ProjectClearCommand());
        registryCommand(new TaskCreateCommand());
        registryCommand(new TaskListCommand());
        registryCommand(new TaskViewCommand());
        registryCommand(new TaskEditCommand());
        registryCommand(new TaskRemoveCommand());
        registryCommand(new TaskClearCommand());
        registryCommand(new ExitCommand());
    }

    private void registryCommand(final AbstractCommand command) {
        if (command.command()==null||command.command().isEmpty()){
            return;
        }
        if (command.description()==null||command.description().isEmpty()){
            return;
        }
        command.setBootstrap(this);
        commands.put(command.command(),command);
    }

    public void start() {
        init();
        String command = "";

        System.out.println("***WELCOME TO TASK MANAGER***");

        while (!commands.get("exit").command().equals(command)) {
            try {
                command = reader.readLine();
                if (!"".equals(command))
                commands.get(command).execute();

            } catch (ParseException e) {
                System.out.println("Wrong input, try again");
            } catch (IndexOutOfBoundsException e) {
                System.out.println("Selected index doesn't exist");
            } catch (Exception e) {
                System.out.println("Something wrong, try again");
            }
        }
    }

    public ProjectRepository getProjectRepository() {
        return projectRepository;
    }

    public TaskRepository getTaskRepository() {
        return taskRepository;
    }

    public ProjectService getProjectService() {
        return projectService;
    }

    public TaskService getTaskService() {
        return taskService;
    }

    public Collection<AbstractCommand> getCommands() {
        return commands.values();
    }
}